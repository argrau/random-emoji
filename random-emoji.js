/*
 * Returns aleatory Emoji from an array
 * @return {string} 
 */
function makeEmoji() {
 // Create an array of Emojis
 var emojiArray = [
  ':dog:',
  ':cat:',
  ':mouse:',
  ':hamster:',
  ':rabbit:',
  ':bear:',
  ':panda_face:',
  ':koala:',
  ':tiger:',
  ':lion_face:',
  ':cow:',
  ':pig:',
  ':frog:',
  ':octopus:',
  ':monkey_face:',
  ':chicken:',
  ':penguin:',
  ':wolf:',
  ':boar:',
  ':horse:',
  ':unicorn_face:'
 ];
 
 // Aleatory Emoji from the array
 const generatedEmoji = myArray[Math.floor(Math.random() * myArray.length)];
 
 // Returns the Emoji
 return generatedEmoji;
}

